/// <reference types="Cypress" />

describe("Registration Page", function() {
  it("lets you fill the form", function() {
    cy.visit("http://localhost:3000/register");
    cy.findByLabelText("First Name").type("jeff");
    cy.findByLabelText("Last Name").type("jeff");
    cy.findByLabelText("Email").type("jeff@test.com");
    cy.findByLabelText("Password").type("Pass1234!");
    cy.findByLabelText("Trainer Experience Level").select("Pro");
    cy.findByText("I accept the terms and conditions").click();
  });
  it("submits", function() {
    cy.findByText("Submit").click();
  });
});
