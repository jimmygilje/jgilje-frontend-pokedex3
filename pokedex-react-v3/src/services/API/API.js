import axios from "axios";

const API = axios.create({
  baseURL: "https://intern-pokedex.myriadapps.com/api/v1"
  // baseURL: "http://localhost:8000/api/v1",
  // headers: {
  //   Authorization:
  //     "Bearer zByAy28VV5oYWYBLR8GHRxX6AeODUZ3BmJ6L9yxgQrCOtV7GFMAyuQVuXyysd4T6ESd6SIbHDJAJw2rh"
  // }
});

export default API;
