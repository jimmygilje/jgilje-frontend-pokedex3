import API from "./API";
import queryString from "query-string";

export const getPokemon = async (page = 1, searchText = null) => {
  var query = {};
  if (searchText) query.name = searchText;
  if (page) query.page = page;
  return await API.get(`/pokemon/?${queryString.stringify(query)}`);
};

export const getPokemonDetail = async id => {
  return await API.get(`/pokemon/${id}`);
};
