import React from "react";
import { BrowserRouter, Route, Switch, Link } from "react-router-dom";
import PokemonDetail from "./views/PokemonDetail";
import PokemonGrid from "./views/PokemonGrid";
import Register from "./views/Register";
import { Box, Flex, ThemeProvider, CSSReset } from "@chakra-ui/core";
import customTheme from "./theme";

function App() {
  return (
    <BrowserRouter>
      <ThemeProvider theme={customTheme}>
        <CSSReset />
        <Box w="100%" bg="#55a69c" minHeight="100vh">
          <Box maxW="1000px" m="0px auto" py={4}>
            <Flex>
              <Link to="/">
                <Flex color="white" mx={10}>
                  Home
                </Flex>
              </Link>
              <Link to="/register">
                <Flex color="white">Register</Flex>
              </Link>
            </Flex>

            <Switch>
              <Route path="/register">
                <Register />
              </Route>
              <Route path="/detail/:id">
                <PokemonDetail />
              </Route>
              <Route path="/">
                <PokemonGrid />
              </Route>
            </Switch>
          </Box>
        </Box>
      </ThemeProvider>
    </BrowserRouter>
  );
}

export default App;
