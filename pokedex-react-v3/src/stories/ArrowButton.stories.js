import React from "react";
import { action } from "@storybook/addon-actions";
import ArrowButton from "../components/ArrowButton";

export default {
  title: "ArrowButton",
  component: ArrowButton
};

export const RightPrimary = () => (
  <ArrowButton isActive variant="primary" callbackFunc={action("clicked")} />
);

export const LeftPrimary = () => (
  <ArrowButton
    isLeft
    isActive
    variant="primary"
    callbackFunc={action("clicked")}
  />
);

export const Secondary = () => (
  <ArrowButton isActive variant="secondary" callbackFunc={action("clicked")} />
);

export const Inactive = () => (
  <ArrowButton
    isActive={false}
    variant="inactive"
    callbackFunc={action("clicked")}
  />
);
