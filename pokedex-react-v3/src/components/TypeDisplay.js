import React from "react";
import { Box } from "@chakra-ui/core";

const TypeDisplay = ({ type }) => {
  const typeColor = getTypeColor(type);
  return (
    <Box
      bg={typeColor.primaryColor}
      border="1px solid"
      borderColor={typeColor.secondaryColor}
      color={typeColor.secondaryColor}
      rounded="5px"
      px="5px"
      m="2"
      fontSize={12}
    >
      {type}
    </Box>
  );
};

function getTypeColor(type) {
  switch (type) {
    case "BUG":
      return { primaryColor: "green.300", secondaryColor: "green.800" };
    case "DRAGON":
      return { primaryColor: "purple.700", secondaryColor: "purple.50" };
    case "ELECTRIC":
      return { primaryColor: "yellow.200", secondaryColor: "yellow.500" };
    case "FIGHTING":
      return { primaryColor: "orange.700", secondaryColor: "orange.50" };
    case "FIRE":
      return { primaryColor: "red.500", secondaryColor: "red.900" };
    case "FLYING":
      return { primaryColor: "teal.300", secondaryColor: "teal.800" };
    case "GHOST":
      return { primaryColor: "purple.300", secondaryColor: "purple.900" };
    case "GRASS":
      return { primaryColor: "green.100", secondaryColor: "green.800" };
    case "GROUND":
      return { primaryColor: "yellow.600", secondaryColor: "yellow.50" };
    case "ICE":
      return { primaryColor: "cyan.100", secondaryColor: "cyan.800" };
    case "NORMAL":
      return { primaryColor: "orange.100", secondaryColor: "orange.800" };
    case "POISON":
      return { primaryColor: "purple.100", secondaryColor: "purple.800" };
    case "PSYCHIC":
      return { primaryColor: "pink.500", secondaryColor: "pink.900" };
    case "ROCK":
      return { primaryColor: "yellow.600", secondaryColor: "yellow.900" };
    case "WATER":
      return { primaryColor: "blue.200", secondaryColor: "blue.900" };
    case "FAIRY":
      return { primaryColor: "pink.300", secondaryColor: "pink.800" };
    case "DARK":
      return { primaryColor: "red.900", secondaryColor: "red.50" };
    case "STEEL":
      return { primaryColor: "gray.600", secondaryColor: "gray.50" };
    default:
      return { primaryColor: "gray.600", secondaryColor: "gray.50" };
  }
}

export default React.memo(TypeDisplay);
