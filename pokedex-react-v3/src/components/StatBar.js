import React from "react";
import { Box, Flex } from "@chakra-ui/core";

const maxStatVal = 250;

const StatBar = ({ label, value }) => {
  const statPercent = (value / maxStatVal) * 100;
  return (
    <Flex my={1} alignItems="flex-start">
      <Box flex={1} fontSize={15}>
        {label}
      </Box>
      <Box bg="#4e988f5c" fontSize={16} flex={2} color="white">
        <Box bg="#4e988f" width={`${statPercent}%`} pl={2}>
          {value}
        </Box>
      </Box>
    </Flex>
  );
};

export default StatBar;
