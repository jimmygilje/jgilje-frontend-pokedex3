/** @jsx jsx */
import { jsx } from "@emotion/core";
import { Box, Flex, Image } from "@chakra-ui/core";

const DetailSectionHeader = ({ children }) => (
  <Box
    bg="#4e988f"
    fontSize={17}
    fontWeight="600"
    width="100%"
    pl={4}
    py={1}
    color="white"
  >
    {children}
  </Box>
);

export default DetailSectionHeader;
