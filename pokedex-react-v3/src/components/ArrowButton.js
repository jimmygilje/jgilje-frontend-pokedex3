/** @jsx jsx */
import { jsx, css } from "@emotion/core";
import styled from "@emotion/styled";
import { FaArrowLeft, FaArrowRight } from "react-icons/fa";
import { Box, Flex, Image } from "@chakra-ui/core";
import { variant } from "styled-system";

const ArrowBtn = styled("button")(
  `
  border-radius: 50%;
  padding: 20px;
  display: flex;
  align-items: center;
  font-size: 18px;
  border: none;
  outline: none;
`,
  variant({
    variants: {
      primary: {
        color: "#fff",
        bg: "#4e988f"
      },
      secondary: {
        color: "#4e988f",
        bg: "#fff"
      },
      inactive: {
        color: "#fff",
        bg: "#4e988f",
        filter: "opacity(0.5)"
      }
    }
  })
);

const ArrowButton = ({
  isActive = true,
  isLeft,
  onClick,
  variant = "primary"
}) => (
  <ArrowBtn disabled={!isActive} onClick={onClick} variant={variant}>
    {isLeft ? <FaArrowLeft /> : <FaArrowRight />}
  </ArrowBtn>
);

export default ArrowButton;
