import React from "react";
import { Field } from "formik";
import {
  FormControl,
  Input,
  FormErrorMessage,
  FormLabel
} from "@chakra-ui/core";

const TextField = ({ label, name, containerProps, ...props }) => {
  return (
    <Field name={name}>
      {({ field, meta }) => (
        <FormControl
          isInvalid={meta.error && meta.touched}
          width="100%"
          mb={3}
          {...containerProps}
        >
          <FormLabel htmlFor={name}>{label}</FormLabel>
          <Input {...field} id={name} placeholder={label} {...props} />
          <FormErrorMessage>{meta.error}</FormErrorMessage>
        </FormControl>
      )}
    </Field>
  );
};

export default TextField;
