import React from "react";
import { Box, Flex, Image } from "@chakra-ui/core";
import TypeDisplay from "./TypeDisplay";
import { Link } from "react-router-dom";

const PokemonCard = ({ pokemon }) => {
  return (
    <Link to={`/detail/${pokemon.id}`}>
      <Box bg="white" p={4} height="100%" borderRadius={3}>
        <Flex justifyContent="flex-start">{pokemon.name}</Flex>
        <Flex justifyContent="center">
          <Image src={pokemon.image} />
        </Flex>
        <Flex justifyContent="flex-end">
          {pokemon.types.map((type, key) => (
            <TypeDisplay key={key} type={type.toUpperCase()}></TypeDisplay>
          ))}
        </Flex>
      </Box>
    </Link>
  );
};

export default PokemonCard;
