import React from "react";
import { Field } from "formik";
import { FormControl, Checkbox, FormErrorMessage } from "@chakra-ui/core";

const CheckboxField = ({ label, name, ...props }) => {
  return (
    <Field name={name}>
      {({ field, meta }) => (
        <FormControl isInvalid={meta.error && meta.touched} width="100%" mb={3}>
          <Checkbox {...field} id={name} {...props} />
          <FormErrorMessage>{meta.error}</FormErrorMessage>
        </FormControl>
      )}
    </Field>
  );
};

export default CheckboxField;
