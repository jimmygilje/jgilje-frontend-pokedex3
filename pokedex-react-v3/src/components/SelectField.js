import React from "react";
import { Field } from "formik";
import {
  FormControl,
  Select,
  FormErrorMessage,
  FormLabel
} from "@chakra-ui/core";

const SelectField = ({ label, name, ...props }) => {
  return (
    <Field name={name}>
      {({ field, meta }) => (
        <FormControl isInvalid={meta.error && meta.touched} width="100%" mb={3}>
          <FormLabel htmlFor={name}>{label}</FormLabel>
          <Select {...field} id={name} {...props} />
          <FormErrorMessage>{meta.error}</FormErrorMessage>
        </FormControl>
      )}
    </Field>
  );
};

export default SelectField;
