import React, { useState } from "react";
import { InputGroup, InputLeftElement, Input, Icon } from "@chakra-ui/core";

const Search = ({ handleSearchSubmit, searchText }) => {
  const [localSearchText, setLocalSearchText] = useState(searchText);
  const handleSearchChange = event => {
    setLocalSearchText(event.target.value);
  };
  return (
    <InputGroup
      bg="#519f95"
      color="#fff"
      flex={3}
      size="lg"
      alignItems="center"
      mx={4}
      my={3}
      borderRadius="5px"
      _placeholder={{ color: "red" }}
    >
      <InputLeftElement top="unset">
        <Icon name="search" />
      </InputLeftElement>
      <form onSubmit={handleSearchSubmit}>
        <Input
          name="searchInput"
          placeholder="Pokedex"
          bg="#519f95"
          fontSize="68px"
          variant="filled"
          p={10}
          focusBorderColor="none"
          borderRadius="5px"
          _placeholder={{ color: "#458880" }}
          fontWeight="700"
          value={localSearchText}
          onChange={handleSearchChange}
        />
      </form>
    </InputGroup>
  );
};

export default Search;
