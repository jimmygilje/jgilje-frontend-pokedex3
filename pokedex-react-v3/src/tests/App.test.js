import React from "react";
import { render } from "@testing-library/react";
import App from "../App";

test("renders with Home and Register links", () => {
  const { getByText } = render(<App />);
  const homeLink = getByText("Home");
  expect(homeLink).toBeInTheDocument();
  expect(homeLink.parentNode.getAttribute("href")).toBe(`/`);
  const registerLink = getByText("Register");
  expect(registerLink).toBeInTheDocument();
  expect(registerLink.parentNode.getAttribute("href")).toBe(`/register`);
});
