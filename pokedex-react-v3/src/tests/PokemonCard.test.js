import React from "react";
import { render } from "@testing-library/react";
import { BrowserRouter } from "react-router-dom";

import PokemonCard from "../components/PokemonCard";
import pokemon from "../../cypress/fixtures/pokemon.json"; //no need to duplicate fixtures in two places

it("renders with pokemon name and a link to pokemon detail", () => {
  const { getByText } = render(
    <BrowserRouter>
      <PokemonCard pokemon={pokemon} />
    </BrowserRouter>
  );
  const name = getByText(pokemon.name);
  expect(name).toBeInTheDocument();
  expect(document.querySelector("a").getAttribute("href")).toBe(
    `/detail/${pokemon.id}`
  );
});
