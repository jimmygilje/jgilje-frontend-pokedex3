/** @jsx jsx */
import React, { useEffect, useState } from "react";
import axios from "axios";
import { jsx } from "@emotion/core";
import { Box, Flex, Image } from "@chakra-ui/core";
import DetailSectionHeader from "../components/DetailSectionHeader";
import StatBar from "../components/StatBar";
import TypeDisplay from "../components/TypeDisplay";
import ArrowButton from "../components/ArrowButton";
import { useHistory, useParams } from "react-router-dom";
import { getPokemonDetail } from "../services/API/pokemon";

const PokemonDetail = () => {
  const [pokemon, setPokemon] = useState(null);
  const history = useHistory();
  const { id } = useParams();
  useEffect(() => {
    getPokemonDetail(id).then(response => {
      setPokemon(response.data.data);
    });
  }, []);

  const handlePrevClick = () => {
    history.goBack();
  };

  return (
    pokemon && (
      <Box>
        <Flex width="100%" py={6}>
          <Box
            flex={3}
            textAlign="center"
            fontSize={68}
            fontWeight="700"
            color="white"
          >
            <Box
              flex={0}
              float="left"
              position="absolute"
              mt={5}
              ml={[4, , 32, 40]}
            >
              <ArrowButton
                isLeft
                isActive
                variant="secondary"
                onClick={handlePrevClick}
              />
            </Box>

            {pokemon.name}
          </Box>
        </Flex>
        <Box bg="white" borderRadius={3} mx={[4, 10, 32, 40]} px={8} py={3}>
          <Flex borderBottom="1px solid #e0e0e0" pb={1}>
            <Box fontWeight={700} alignSelf="center">
              {pokemon.name}
            </Box>
            <Box fontWeight={700} alignSelf="center" color="#808080ba" ml={4}>
              #{pokemon.id}
            </Box>

            <Flex flex={1} justifyContent="flex-end !important">
              {pokemon.types &&
                pokemon.types.map((type, key) => (
                  <TypeDisplay key={key} type={type.toUpperCase()} />
                ))}
            </Flex>
          </Flex>
          <Flex my={4}>
            <Flex flex={1} alignItems="center">
              <img src={pokemon.image} />
            </Flex>
            <Box flex={2}>
              {pokemon.stats &&
                Object.entries(pokemon.stats).map(([label, value], key) => (
                  <StatBar key={key} label={label} value={value}></StatBar>
                ))}
            </Box>
          </Flex>
          <Flex justifyContent="flex-start !important" fontWeight={700}>
            {pokemon.genus}
          </Flex>
          <Flex>
            <p>{pokemon.description}</p>
          </Flex>
          <Flex my={4}>
            <DetailSectionHeader>Profile</DetailSectionHeader>
          </Flex>
          <Box mx={10}>
            <Flex>
              <Box flex={1} fontWeight={700}>
                Height:
              </Box>
              <Box flex={1}>{pokemon.height} m</Box>
              <Box flex={1} fontWeight={700}>
                Weight:
              </Box>
              <Box flex={1}>{pokemon.weight} kg</Box>
            </Flex>
            <Flex>
              <Box flex={1} fontWeight={700}>
                Egg Groups:
              </Box>
              <Box flex={1}>
                {pokemon.egg_groups && pokemon.egg_groups.join(", ")}
              </Box>
              <Box flex={1} fontWeight={700}>
                Abilities:
              </Box>
              <Box flex={1}>
                {pokemon.abilities && pokemon.abilities.join(", ")}
              </Box>
            </Flex>
          </Box>
        </Box>
      </Box>
    )
  );
};

export default PokemonDetail;
