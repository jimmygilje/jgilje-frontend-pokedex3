import React from "react";
import { Formik, Form, Field, useField } from "formik";
import { Button, Heading, Flex, Box } from "@chakra-ui/core";
import * as Yup from "yup";

import SelectField from "../components/SelectField";
import TextField from "../components/TextField";
import CheckboxField from "../components/CheckboxField";

const Register = () => {
  return (
    <Box>
      <Heading color="white" textAlign="center">
        Register a trainer account
      </Heading>
      <Flex justify="center">
        <Formik
          initialValues={{
            firstName: "",
            lastName: "",
            email: "",
            password: "",
            acceptedTerms: false,
            experienceLevel: ""
          }}
          validationSchema={Yup.object({
            firstName: Yup.string()
              .max(30, "Must be 30 characters or less")
              .required("Required"),
            lastName: Yup.string()
              .max(30, "Must be 30 characters or less")
              .required("Required"),
            email: Yup.string()
              .email("Invalid email address")
              .required("Required"),
            password: Yup.string()
              .min(8, "Password must be at least 8 characters")
              .required("Required"),
            acceptedTerms: Yup.boolean()
              .required("Required")
              .oneOf([true], "You must accept the terms and conditions."),
            experienceLevel: Yup.string()
              .oneOf(
                ["novice", "pro", "awesome", "awesomer"],
                "Invalid Experience Level"
              )
              .required("Required")
          })}
          onSubmit={(values, { setSubmitting }) => {
            setTimeout(() => {
              alert(JSON.stringify(values, null, 2));
              setSubmitting(false);
            }, 400);
          }}
        >
          {props => (
            <Form>
              <Box p={5} mt={3} backgroundColor="#fff" borderRadius={3}>
                <TextField
                  label="First Name"
                  name="firstName"
                  type="text"
                  containerProps={{ flex: 1 }}
                />
                <TextField label="Last Name" name="lastName" type="text" />
                <TextField label="Email" name="email" type="email" />
                <TextField label="Password" name="password" type="password" />
                <SelectField
                  label="Trainer Experience Level"
                  placeholder="Select Experience Level"
                  name="experienceLevel"
                >
                  <option value="novice">Novice</option>
                  <option value="pro">Pro</option>
                  <option value="awesome">Awesome</option>
                  <option value="awesomer">Awesomer</option>
                </SelectField>
                <CheckboxField name="acceptedTerms">
                  I accept the terms and conditions
                </CheckboxField>

                <Button
                  mt={4}
                  variantColor="teal"
                  isLoading={props.isSubmitting}
                  type="submit"
                >
                  Submit
                </Button>
              </Box>
            </Form>
          )}
        </Formik>
      </Flex>
    </Box>
  );
};

export default Register;
