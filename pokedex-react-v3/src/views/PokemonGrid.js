import React, { useState, useEffect } from "react";
import { Box, Grid, Flex } from "@chakra-ui/core";
import PokemonCard from "../components/PokemonCard";
import Search from "../components/Search";
import ArrowButton from "../components/ArrowButton";
import { useHistory, useLocation, Link } from "react-router-dom";
import queryString from "query-string";
import { getPokemon } from "../services/API/pokemon";

const PokemonGrid = () => {
  const [pokemonData, setPokemonData] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const history = useHistory();
  const location = useLocation();
  let { page = 1, searchText } = queryString.parse(location.search);

  useEffect(() => {
    refresh();
  }, [location.search]);

  const refresh = () => {
    getPokemon(page, searchText).then(response => {
      setPokemonData(response.data.data);
      setIsLoaded(true);
    });
  };

  const handleSearchSubmit = event => {
    event.preventDefault();
    const searchValue = event.target.elements.searchInput.value;
    if (searchValue) {
      history.push(`?searchText=${searchValue}`);
    } else {
      history.push(`/`);
    }
  };

  const navigateToPage = () => {
    const values = { page, searchText };
    history.push(`?${queryString.stringify(values)}`);
  };

  const handlePrevClick = () => {
    page = isNaN(parseInt(page)) ? 1 : parseInt(page) - 1;
    navigateToPage();
  };

  const handleNextClick = () => {
    page = isNaN(parseInt(page)) ? 1 : parseInt(page) + 1;
    navigateToPage();
  };

  return (
    <Box>
      <Flex mb={4}>
        <Flex flex={1} alignItems="center" justify="center">
          <ArrowButton
            isLeft
            isActive={page > 1}
            variant={page <= 1 ? "inactive" : "primary"}
            onClick={handlePrevClick}
          />
        </Flex>

        <Search
          handleSearchSubmit={handleSearchSubmit}
          searchText={searchText}
        ></Search>
        <Flex flex={1} alignItems="center" justify="center">
          <ArrowButton onClick={handleNextClick} />
        </Flex>
      </Flex>

      <Grid
        templateColumns="repeat(auto-fill, 220px)"
        gap="3"
        justifyContent="center"
        alignItems="stretch"
        m="0 16px"
      >
        {isLoaded
          ? pokemonData.map(pokemon => (
              <PokemonCard key={pokemon.id} pokemon={pokemon} />
            ))
          : "Loading..."}
      </Grid>
    </Box>
  );
};

export default PokemonGrid;
